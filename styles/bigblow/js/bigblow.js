console.log("first line");
window.onload = function() {
    // we collect the nodes where we want to add eventlistener-buttons to roll-up and roll-down content headers.
    // Set it to the header numbers, not the header text itself.
    var nodeListmapa = document.querySelectorAll("span.section-number-1, span.section-number-2, span.section-number-3, span.section-number-4,  span.section-number-5,  span.section-number-6, span.section-number-7, span.section-number-8, span.section-number-9, span.section-number-10, span.section-number-11, span.section-number-12, span.section-number-13, span.section-number-14, span.section-number-15, span.section-number-16");

    // first we collect the nodes we want to show initially when the page loads.
    var nodeListmapa2 = document.querySelectorAll("span.section-number-1, span.section-number-2");
    var topHeadersNodes = document.querySelectorAll("#text-table-of-contents > ul > li > a");
    console.log(topHeadersNodes);
    function hasClass(element, clsName) {
	return (' ' + element.className + ' ').indexOf(' ' + clsName + ' ') > -1;
    }
    function toggleinvisiblerecursively(element){
	console.log("toggling for...");
	element.classList.toggle('invisible');
	console.log(element.className)	
	if (element.children){
	    for (const child of element.children){
		toggleinvisiblerecursively(child);
	    }
	}
    }
    
    nodeListmapa.forEach((btn) => {
	console.log("inside nodelist adding buttons");
	btn.addEventListener('click', (e) => {
	    console.log("inside nodelist adding eventlistener");
	    var fparent = e.target.parentElement;
	    var sparent = fparent.parentElement;
	    for (const child of sparent.children){
		console.log(child); // section-number-N invisible
		if (hasClass(child, 'invisible')){ // should include <div outline-N invisible class, but might also be outline-text-N elements.
		    console.log('child has class invisible');
		    child.classList.toggle('invisible'); // make this outline class header and span visible
		    let pattern = /outline-[0-9]+/;
		    if (pattern.test(child.className)){
			console.log('child has className outline-N');
			//child.classList.toggle('invisible'); // make this outline class visible - doesn't include it's header and span or any other child elements
			if (child.children){
			    console.log('child has children');
			    console.log(child.children);

			    child.children.item(0).classList.toggle('invisible');
			    console.log(child.children.item(0));

			    child.children.item(0).children.item(0).classList.toggle('invisible');

			    child.children.item(1).classList.toggle('invisible');
			    console.log(child.children.item(1));
			}
		    } else { // if the classname is not outline-N, it's some div with paragraphs that we need to toggle recursively.
			for (otherchild of child.children){
			    toggleinvisiblerecursively(otherchild);
			}
		    }
		} else if (child === e.target){
		} else if (child === fparent){
		} else {
		    child.classList.toggle('invisible');
		    console.log('child doesn\'t have class invisible');
		    let pattern = /outline-[0-9]+/;
		    if (pattern.test(child.className)){
			console.log('child has className outline-N');
			//child.classList.toggle('invisible'); // make this outline class visible - doesn't include it's header and span or any other child elements
			if (child.children){
			    console.log('child has children');
			    console.log(child.children);

			    child.children.item(0).classList.toggle('invisible');
			    console.log(child.children.item(0));

			    child.children.item(0).children.item(0).classList.toggle('invisible');

			    child.children.item(1).classList.toggle('invisible');
			    console.log(child.children.item(1));
			}
		    } else { // if the classname is not outline-N, it's some div with paragraphs that we need to toggle BACK recursively.
			toggleinvisiblerecursively(child);
		    }
		}
	    }
	});
    });
    nodeListmapa2.forEach((btn) => {
	var fparent = btn.parentElement;
	var sparent = fparent.parentElement;
	for (const child of sparent.children){
		toggleinvisiblerecursively(child);
	}
	fparent.classList.toggle('invisible');
	btn.classList.toggle('invisible');
    });
    console.log(topHeadersNodes);
    // this version is the same as above, except that it adds the button to the header text elements, not just the numbers.
    nodeListmapa.forEach((btnnum) => {
	console.log("inside nodelist adding buttons for header text");
	btn = btnnum.parentElement;
	btn.addEventListener('click', (e) => {
	    console.log("inside nodelist adding eventlistener");
	    console.log(btn);
	    //var fparent = e.target.parentElement;
	    //var sparent = fparent.parentElement;
	    var sparent = e.target.parentElement;
	    console.log("next log is sparent");
	    console.log(sparent);
	    for (const child of sparent.children){
		console.log(child); // section-number-N invisible
		if (hasClass(child, 'invisible')){ // should include <div outline-N invisible class, but might also be outline-text-N elements.
		    console.log('child has class invisible');
		    child.classList.toggle('invisible'); // make this outline class header and span visible
		    let pattern = /outline-[0-9]+/;
		    if (pattern.test(child.className)){ // it's an outline container <div
			console.log('child has className outline-N');
			//child.classList.toggle('invisible'); // make this outline class visible - doesn't include it's header and span or any other child elements
			if (child.children){
			    console.log('child has children');
			    console.log(child.children);

			    child.children.item(0).classList.toggle('invisible');
			    console.log(child.children.item(0));

			    child.children.item(0).children.item(0).classList.toggle('invisible');

			    child.children.item(1).classList.toggle('invisible');
			    console.log(child.children.item(1));
			}
		    } else { // if the classname is not outline-N, it's some div with paragraphs that we need to toggle recursively.
			for (otherchild of child.children){
			    toggleinvisiblerecursively(otherchild);
			}
		    }
		} else if (child === e.target){
		//} else if (child === fparent){
		} else {
		    child.classList.toggle('invisible');
		    console.log('child doesn\'t have class invisible');
		    let pattern = /outline-[0-9]+/;
		    if (pattern.test(child.className)){
			console.log('child has className outline-N');
			//child.classList.toggle('invisible'); // make this outline class visible - doesn't include it's header and span or any other child elements
			if (child.children){
			    console.log('child has children');
			    console.log(child.children);

			    child.children.item(0).classList.toggle('invisible');
			    console.log(child.children.item(0));

			    child.children.item(0).children.item(0).classList.toggle('invisible');

			    child.children.item(1).classList.toggle('invisible');
			    console.log(child.children.item(1));
			}
		    } else { // if the classname is not outline-N, it's some div with paragraphs that we need to toggle BACK recursively.
			toggleinvisiblerecursively(child);
		    }
		}
	    }
	});
    });

};
